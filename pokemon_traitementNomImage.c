#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#define LIGNE 2
#define COLONNE 1

int main (void){

  char **pokemon;
  int i;

  /* *** allocation du tableau de positionnement *** */
  pokemon = calloc(LIGNE, sizeof(char));

  if(pokemon == NULL){
    fprintf(stderr, "erreur du pokemon");
    exit(EXIT_FAILURE);
  }

  for(i = 0; i < LIGNE; i++){
    pokemon[i] = calloc(COLONNE, sizeof(char));

    if(pokemon == NULL){
      fprintf(stderr, "erreur du pokemon");
      exit(EXIT_FAILURE);
    }
  }
  /* ** fin d'allocation du tableau de positionnement ** */

  /* *** Entree de donnees *** */
  // Premiere loop = positionnement
  // Deuxieme loop = nom de l'image
  puts("ligne 0 = positionnement (front/back); ligne 1 = nom image + .png");
  puts("---");
  for(i = 0; i < LIGNE; i++){
    printf("ligne %d : ", i);
    scanf("%s", pokemon[i]);
  }
  /* ** fin de la saisit de donnees ** */

  /* *** Affichage + deallocation *** */
  // affichage
  //strcmp(temp, "mystring")
  if(strcmp(pokemon[0], "front") && strcmp(pokemon[0], "back")){
        fprintf(stderr, "erreur saisit pokemon : %s",  pokemon[0]);
        exit(EXIT_FAILURE);
      }
        printf("image => %s-%d.png\n",pokemon[0], atoi(pokemon[1]));


  // deallocation
  for(i=0; i < LIGNE; i++)
      free(pokemon[i]);

  free(pokemon);
  /* ** fin affichage + deallocation ** */

  return (EXIT_SUCCESS);
}
